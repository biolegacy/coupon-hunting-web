export default {
    setActivePage: (dispatch, page) => {
        console.log("Page: ", page);
        dispatch({
            type: "updatePaginationPage",
            activePage: page
        })
    }
}