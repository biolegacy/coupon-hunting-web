export const CartItems = [
    {
        info:{
            id: 15,
            image: "",
            name: "Product 1",
            description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard",
            price: 1500,
            tier: "Silver"
        },
        quantity: 1
    },
    {
        info:{
            id: 22,
            image: "",
            name: "Product 2",
            description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard",
            price: 1800,
            tier: "Silver"
        },
        quantity: 2
    },
    {
        info:{
            id: 23,
            image: "",
            name: "Product 4",
            description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard",
            price: 750,
            tier: "Bronze"
        },
        quantity: 5
    }
]

export const WishListItems = [
    {
        info:{
            id: 33,
            image: "",
            name: "Product 1",
            description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard",
            price: 2500,
            tier: "Gold"
        },
        quantity: 2
    },
    {
        info:{
            id: 36,
            image: "",
            name: "Product 2",
            description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard",
            price: 5500,
            tier: "Gold"
        },
        quantity: 3
    },
    {
        info:{
            id: 42,
            image: "",
            name: "Product 4",
            description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard",
            price: 12500,
            tier: "Premium"
        },
        quantity: 2
    }
]