import React from "react";

// Importing third party components
import { connect } from "react-redux";

// Importing custom components
import HeaderTop from "../../components/menu/top"; 

class Index extends React.Component{
    constructor(props){
        super(props);
        this.state={

        }
    }

    render(){
        return(
            <div id="coupon-page">
                <HeaderTop 
                    history={this.props.history}
                />
                <h1>View Coupon</h1>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({

});

const ViewCoupon = connect(mapStateToProps)(Index);

export default ViewCoupon;