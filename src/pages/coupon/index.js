import React from "react";

// Importing style
import "../../style/css/coupons-page.css";

// Importing third party components
import { connect } from "react-redux"; 
import { Link } from "react-router-dom";
import Paginate from "react-paginate";

// Importing custom components
import HeaderTop from "../../components/menu/top";

// Importing Utility ( Helper functions )
import utility from "../../utility";

class Index extends React.Component{
    constructor(props){
        super(props);
        this.state={

        }
    }

    render(){
        return(
            <div id="coupons-page">
                <HeaderTop 
                    history={this.props.history}
                />
                <div id="list-wrapper">
                    <div className="coupon">
                        <div className="img-wrapper">
                            <Link to={`/coupon/1`}>
                                <img 
                                    src="https://image.shutterstock.com/image-vector/vector-design-elements-your-company-260nw-709133980.jpg" 
                                    alt={`Random text`}
                                />
                            </Link>
                        </div>
                        <ul className="details-wrapper">
                            <li className="detail">
                                Owner: <Link to={`/coupon/1`}>Company 1</Link>
                            </li>
                            <li className="detail">
                                Industry: <span>Restraunt</span>
                            </li>
                            <li className="detail">
                                Tier: <span>3</span>
                            </li>
                            <li className="detail">
                                Price: <span>$9.99</span>
                            </li>
                            <li className="view-btn">
                                <Link to={`/coupon/${1}`}>Visit</Link> 
                            </li>
                        </ul>
                    </div>
                </div>
                <Paginate
                    initialPage={this.props.pagination.activePage} 
                    pageCount={20}
                    pageRangeDisplayed={5}
                    previousLabel="<"
                    nextLabel=">"
                    containerClassName="pagination-container"
                    pageClassName="pagination-item"
                    previousClassName="pagination-item"
                    nextClassName="pagination-item"
                    breakClassName="pagination-break"
                    activeClassName="pagination-active"
                    onPageChange={(value)=>{ utility.setActivePage(this.props.dispatch, value.selected) }}
                />
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    pagination: state.Pagination
})

const CouponsPage = connect(mapStateToProps)(Index);

export default CouponsPage;