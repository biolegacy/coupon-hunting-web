import React from "react";

// Importing styles
import "../../style/css/cart.css";

// Importing third party components
import { connect } from "react-redux";
import { Link } from "react-router-dom";

// Importing custom components
import HeaderTop from "../../components/menu/top";
import CartMenu from "../../components/menu/cart-menu";

// Importing Cart reducer Actions
import { Actions, ItemType } from "../../service/reducers/cart";

// Importing MockData
import { CartItems, WishListItems } from "../../mock_data/cart";

class Index extends React.Component{
    constructor(props){
        super(props);
        this.state={
            refresh: true
        }
    }

    componentDidMount(){
        // Fetch the list items and set

        // Set the cart and wishlist items
        this.props.dispatch((dispatch) => {
            dispatch({
                type: Actions.intialSet,
                payload: {
                    cartItems: CartItems,
                    wishlistItems: WishListItems
                }
            });
        })
    }

    /**
     * @function refresh refreshes the page when there has been a chance in props not in the page state
     */
    refresh(){
        this.setState({ 
            refresh: true
        })
    }

    /**
     * 
     * @param { string || int } id 
     */
    remove(id, index, itemType){
        console.log("Remove a product with ID of: ", id);
        // Remove the item from the respective list based on the `itemType`
        // remove the item from the list on the local data
        this.props.dispatch(dispatch => {
            dispatch({
                type: Actions.remove,
                payload:{
                    index,
                    itemType
                }
            })
            this.refresh();
        })
    }

    /**
     * 
     * @param { string || int }
     */
    moveToCart(id, index){
        console.log(`Move a product with ID of: ${id} to the cart`);
        // Move the item from wishlist to cart
        this.props.dispatch(dispatch => {
            dispatch({
                type: Actions.moveToCart,
                payload: {
                    index
                }
            });
            this.refresh();
        })
    }

    /**
     * 
     * @param { string || int } id
     */
    moveToWishList(id, index){
        console.log(`Move a product with ID of: ${id} to the wishlist`);
        // Move the item from cart to wishlist
        this.props.dispatch(dispatch => {
            dispatch({
                type: Actions.moveToWishlist,
                payload: {
                    index
                }
            });
            this.refresh();
        })
    }

    /**
     * 
     * @param { string || int } id
     */
    updateQuantity(id, quantity, index, itemType){
        console.log("Update the quantity of the product with id of: ", id, "to :", quantity);
        // Right now it's using constant value but this value should be the updated value that came from the API
        
        // Update the quantity of this specific product in the Redux state
        this.props.dispatch(dispatch => {
            dispatch({
                type: Actions.updateQuantity,
                payload:{
                    index,
                    itemType,
                    quantity
                }
            })
            this.refresh();
        })
    }

    /**
     * @function buildFromTemplate builder function for cart / wishlist item
     * @param { object } product which resides in cart / wishlist
     * @param { int } index of the item in the array 
     */
    buildFromTemplate(product, index, itemType){
        return(
            <li className="item" key={index}>
                        <div className="details-wrapper">
                            <div className="img-wrapper">
                                <Link to="">
                                    <img 
                                        src="https://www.jcpportraits.com/sites/jcpportraits.com/files/coupon/2001/core-offer-web.jpg" 
                                        alt="coupon" 
                                    />
                                </Link>
                            </div>
                            <div className="info">
                                <h6>{product.info.name}</h6>
                                <p>{product.info.description}</p>
                                <div className="price"><span>Price:</span> { product.info.price } yen</div>
                            </div>
                            <ul className="calculation">
                                <li>
                                    Quantity
                                    <input  className="quantity-control"
                                            type="number" 
                                            placeholder="0" 
                                            defaultValue={product.quantity} 
                                            onChange={(e) => { 
                                                this.updateQuantity(product.info.id, e.target.value, index, itemType) 
                                            }}
                                    />
                                </li>
                                <li>
                                    <span>Total of:</span> { product.info.price * product.quantity } yen
                                </li>
                            </ul>
                        </div>
                        <div className="controls-wrapper">
                            <CartMenu 
                                id={product.id}
                                index={index}
                                itemType={itemType}
                                remove={(id, index, itemType) => { this.remove(id, index, itemType)}}
                                moveToCart={(id, index) => { this.moveToCart(id, index) }}
                                moveToWishlist={(id, index)=>{ this.moveToWishList(id, index) }}
                            />
                        </div>
                    </li>
        )
    }

    /**
     * @function renderCartItems should render the cart items if there is any
     */
    renderCartItems(){
        if(this.props.cart.items.length > 0){
            return this.props.cart.items.map((product, index) => {
                return this.buildFromTemplate(product, index, ItemType.cart);
            })
        } else {
            return( <h5>No items in the cart!</h5>)
        }
    }

    /**
     * @function renderWishlistItems should render the products in the wishlist if there is any
     */
    renderWishlistItems(){
        if(this.props.cart.wishlist.length > 0){
            return this.props.cart.wishlist.map((product, index) => {
                return this.buildFromTemplate(product, index, ItemType.wishlist);
            })
        } else {
            return( <h5>No items in the wishlist!</h5>)
        }
    }

    /**
     * @function proceedToCheckout will navigate the user to checkout page
     */
    proceedToCheckout(){
        this.props.history.push("/checkout");
    }

    calculateTotal(){
        var totalAmount = 0;
        this.props.cart.items.forEach((item) => {
            totalAmount += (item.info.price * item.quantity);
        });
        return totalAmount;
    }

    render(){
        return(
            <div id="cart-page">
                <HeaderTop 
                    history={this.props.history}
                />
                <div className="with-sub-column width-80 margin-top-50" >
                    <div className="main">
                        <div id="cart">
                            <h4 className="title" >Coupons</h4>
                            <ul className="coupon-list">
                                { this.renderCartItems() }
                            </ul>
                            <h4 className="title wishlist-title" >Wishlist</h4>
                            <ul className="wishlist">
                                { this.renderWishlistItems() }
                            </ul>
                        </div>
                    </div>
                    <div className="sub">
                        <div id="calculation-total">
                            <p>Sub total ( { this.props.cart.items.length } items ): { this.calculateTotal() } yen</p>
                            <div className="checkout-button"
                                onClick={() => { this.proceedToCheckout() }}
                            >
                                Proceed to checkout
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    cart: state.Cart
})

const CartPage = connect(mapStateToProps)(Index);

export default CartPage;

