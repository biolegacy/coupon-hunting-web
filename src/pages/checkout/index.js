import React from "react";

// Importing styles
import "../../style/css/checkout.css";

// Importing third-party components
import { connect } from "react-redux";

// Importing custom components
import HeaderTop from "../../components/menu/top"; 

class Index extends React.Component{
    constructor(props){
        super(props);
        this.state={

        }
    }

    render(){
        return(
            <div id="checkout-page">
                <HeaderTop 
                    history={this.props.history}
                />
                <div className="info-wrapper">
                    <div className="review-order-wrapper">
                        <h4 className="title">Review order</h4>
                        <p className="info">By clicking the “Place your order” button, you place your order for the item after agreeing to Amazon.co.jp's Terms and Conditions, privacy notice, other terms of sales described on item’s detail page and campaign page, if any, and shipping charges and order total. If you place an order using Paidy Invoicing payment method, you also agree to Paidy's terms of use & use of personal information policy.</p>
                        <p className="info">If you place an order for an Amazon Marketplace item, you agree to the third party seller's terms for returns and exchange and Privacy Policy and other terms of sale, shipping charges and conditions, and receiving information on products and services from certain third party sellers. Please confirm the corporate information and location of domestic and foreign sellers and details of their terms and policies on the seller detail page.</p>
                    </div>
                    <div className="info-input-wrapper">
                        <div className="title-wrapper">
                            <h4 className="title">Address info</h4>
                        </div>
                        <div className="address-info">
                            <div className="section-header">
                                Shipping address 
                                <label onClick={()=>{ alert("Change address")}}>Change</label>
                            </div>
                            <label className="name">Nomio Ganbat</label>
                            <label className="post">170-0005</label>
                            <label className="address">Tokyo-to 東京都渋谷区渋谷1-3-9ヒューリック渋谷一丁目ビル 7階</label>
                        </div>
                        <div className="payment-info">
                            <div className="section-header">
                                Payment method 
                                <label onClick={()=>{ alert("Change payment address")}}>Change</label>
                            </div>
                            <label className="">ending in 5279</label>
                        </div>
                        <div className="billing-address-info">
                            <div className="section-header">
                                Billing address 
                                <label onClick={()=>{ alert("Change Billing address")}}>Change</label>
                            </div>
                            <label className="name">Nomio Ganbat</label>
                            <label className="post">170-0005</label>
                            <label className="address">東京都 豊島区東池袋 5-25-10 Tokyo</label>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    checkout: state.Checkout
});

const CheckoutPage = connect(mapStateToProps)(Index);

export default CheckoutPage;