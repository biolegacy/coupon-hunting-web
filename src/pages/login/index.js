import React from "react";

// Importing components
import Input from "../../components/form/input";

const InputTypes = {
    username: "username",
    password: "password"
}

class Index extends React.Component{
    constructor(props){
        super(props);
        this.state={
            username: '',
            password: '',
        }
    }
    /**
     * 
     * @param {String} value
     * @param {String} type can be either Username or Password
     */
    updateValue(value, type){
        switch(type){
            case InputTypes.username: {
                this.setState({
                    username: value
                });
                break;
            }
            case InputTypes.password: {
                this.setState({
                    password: value
                });
                break;
            }
            default:
                break;
        }
    }

    login(){
    }

    render(){
        return(
            <div>
                <h1>Login Page</h1>
                <Input 
                    type="text"
                    placeholder="Username"
                    valueType={InputTypes.username}
                    value={this.state.username}
                    updateValue={(value, type) => { this.updateValue(value, type) }}
                />
                <Input 
                    type="password"
                    placeholder="password"
                    valueType={InputTypes.password}
                    value={this.state.password}
                    updateValue={(value, type) => { this.updateValue(value, type) }}
                />
                <button onClick={()=>{ this.login() }}>Login</button>
            </div>
        )
    }
}

export default Index;