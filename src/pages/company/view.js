import React from "react";

// Importing third party components
import { connect } from "react-redux";

// Importing custom component
import HeaderTop from "../../components/menu/top";


class Index extends React.Component{
    constructor(props){
        super(props);
        this.state={

        }
    }

    render(){
        return(
            <div>
                <HeaderTop 
                    history={this.props.history}
                />
            </div>
        )
    }
}

const mapStateToProps = (state) => ({

});

const ViewCompany = connect(mapStateToProps)(Index);

export default ViewCompany;