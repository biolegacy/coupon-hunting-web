const InitialState = {
    activePage: 0
}

const PaginationReducer = (state= InitialState, action) => {
    console.log("Action: ", action);
    if(action.type === "updatePaginationPage"){
        return {
            ...state,
            activePage: action.activePage
        }
    }
    return state;
}

export default PaginationReducer;