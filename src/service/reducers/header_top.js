const InitialState = {
    activeHeader: "companies"
};

const HeaderTop = (state = InitialState, action) => {
    if(action.type === "update"){
        state.activeHeader = action.activeHeader;
        return state;
    }
    return state;
};

export default HeaderTop;