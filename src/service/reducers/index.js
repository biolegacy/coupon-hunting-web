import { combineReducers } from "redux";

// Importing reducers
import HeaderTop from "./header_top";
import PaginationReducer from "./pagination";
import CartReducer from "./cart";
import CheckoutReducer from "./checkout";

export default combineReducers({
    HeaderTop: HeaderTop,
    Pagination: PaginationReducer,
    Cart: CartReducer,
    Checkout: CheckoutReducer
});