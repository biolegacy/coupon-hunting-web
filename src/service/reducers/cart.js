const InitialState = {
    items: [],
    wishlist: []
}

const CartReducer = (state = InitialState, action) => {
    switch(action.type){
        case Actions.intialSet: {
            return {
                ...state,
                items: action.payload.cartItems,
                wishlist: action.payload.wishlistItems
            };
        }
        case Actions.updateQuantity: {
            const { payload } = action;
            const list = payload.itemType === ItemType.cart ? state.items : state.wishlist;
            // Find the item from the selected list by id
            list[payload.index].quantity = payload.quantity ? payload.quantity : 0;
            // Update the updated list
            if(payload.itemType === ItemType.cart)
                state.items = list;
            else 
                state.wishlist = list;
            return state;
        }
        case Actions.moveToWishlist:{
            const { payload } = action;
            const movingItem = state.items.splice(payload.index, 1);
            state.wishlist.push(movingItem[0]);
            return state;
        }
        case Actions.moveToCart:{
            const { payload } = action;
            const movingItem = state.wishlist.splice(payload.index, 1);
            state.items.push(movingItem[0]);
            return state;
        }
        case Actions.remove:{
            const { payload } = action;
            if(payload.itemType === ItemType.cart)
                state.items.splice(payload.index, 1);
            else 
                state.wishlist.splice(payload.index, 1);
            console.log("state: ", state);
            return state;
        }
        default: return state;
    }
}

export const Actions = {
    intialSet: "intialSet",
    updateQuantity: "updateQuantity",
    moveToWishlist: "moveToWishlist",
    moveToCart: "moveToCart",
    remove: "remove",
}

export const ItemType = {
    cart: "cart",
    wishlist: "wisthlist"
}

export default CartReducer;