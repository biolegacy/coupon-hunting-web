import React from 'react';
import { BrowserRouter, Route } from "react-router-dom";

// Importing Pages
import Login from "./pages/login";

// Company pages
import CompanyHome from "./pages/company";
import ViewCompany from "./pages/company/view";

// Coupons
import Coupons from "./pages/coupon";
import ViewCoupon from "./pages/coupon/view";

// Cart
import Cart from "./pages/cart";

// Checkout
import Checkout from "./pages/checkout";

// Redux essential imports
import { Provider } from "react-redux";
// Import store
import Store from "./service/store";

class Index extends React.Component{
  constructor(props){
    super(props);
    this.state={

    }
  }

  render(){
    return(
      <BrowserRouter>
        <Route exact path="/login" component={ Login }  />
        <Route exact path="/companies" component={ CompanyHome } />
        <Route exact path="/company/:id" component={ ViewCompany } />
        <Route exact path="/coupons" component={ Coupons } />
        <Route exact path="/our-coupons" component={ Coupons } />
        <Route exact path="/coupon/:id" component={ ViewCoupon } />
        <Route exact path="/cart" component={ Cart } />
        <Route exact path="/checkout" component={ Checkout } />
      </BrowserRouter>
    )
  }
}

export default () => {
  return(
    <Provider store={ Store }>
      <Index />
    </Provider>
  )
};
