import React from "react";
import "../../style/css/components/cart_menu.css";

// Importing cart item types
import { ItemType } from "../../service/reducers/cart";

class Index extends React.Component{
    constructor(props){
        super(props);
        this.state={

        }
    }

    /**
     * @function remove removes a product from whishlist/cart 
     */
    remove(){
        this.props.remove(this.props.id, this.props.index, this.props.itemType);
    }

    /**
     * @function moveToCart moves a product from wishlist to cart
     */
    moveToCart(){
        if(this.props.itemType === ItemType.wishlist)
            this.props.moveToCart(this.props.id, this.props.index);
    }

    /**
     * @function moveToWishlist moves a product from cart to wishlist
     */
    moveToWishlist(){
        if(this.props.itemType === ItemType.cart)
            this.props.moveToWishlist(this.props.id, this.props.index);
    }

    render(){
        return(
            <ul className="cart-menu">
                <li onClick={()=>{ this.remove()}} >Remove</li>
                <li onClick={()=>{ this.moveToCart()}} >Move to cart</li>
                <li onClick={()=>{ this.moveToWishlist()}} >Move to wish list</li>
            </ul>
        )
    }
}

export default Index;