import React from "react";
import { connect } from "react-redux";
// Importing styles
import "../../style/css/components/header_top.css";

const Options = {
    companies: "companies",
    coupons: "coupons",
    us: "us",
    ourCoupons: "ourCoupons",
    cart: "cart"
}

class Index extends React.Component{
    constructor(props){
        super(props);
        this.state={

        }
    }

    setActive(option){
        console.log("This.props: ", this.props);
        this.props.dispatch(dispatch => {
            dispatch({
                type: "update",
                activeHeader: option 
            });
            option === Options.ourCoupons ? this.props.history.push(`/our-coupons`) : this.props.history.push(`/${option}`);
        })
    }

    logout(){
        console.log("Logout!");
    }

    render(){
        return(
            <div id="header">
                <nav id="head-menu">
                    <ul>
                        <li onClick={()=>{ this.setActive(Options.companies) }}
                            className={`${this.props.headerTop.activeHeader === Options.companies ? 'active' : null}`}>
                                Companies
                        </li>
                        <li onClick={()=>{ this.setActive(Options.coupons) }}
                            className={`${this.props.headerTop.activeHeader === Options.coupons ? 'active' : null}`}>
                                Coupons
                        </li>
                        <li onClick={()=>{ this.setActive(Options.us) }}
                            className={`${this.props.headerTop.activeHeader === Options.us ? 'active' : null}`}>
                                Us
                        </li>
                        <li onClick={()=>{ this.setActive(Options.ourCoupons) }}
                            className={`${this.props.headerTop.activeHeader === Options.ourCoupons ? 'active' : null}`}>
                                Our coupons
                        </li>
                        <li onClick={()=>{ this.setActive(Options.cart) }}
                            className={`${this.props.headerTop.activeHeader === Options.cart ? 'active' : null}`}>
                                Cart
                        </li>
                        <li onClick={()=>{  }}>Logout</li>
                    </ul>
                </nav>
            </div>
        )
    }
}


const mapStateToProps = (state) => ({
    headerTop: state.HeaderTop
});

const HeaderMenu = connect(mapStateToProps)(Index);

export default HeaderMenu;