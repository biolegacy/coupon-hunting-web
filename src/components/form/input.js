import React from "react";

class Index extends React.Component{
    constructor(props){
        super(props);
        this.state={

        }
    }

    updateValue(value, valueType){
        this.props.updateValue(value, valueType);
    }

    render(){
        return(
            <input 
                type={
                    `${this.props.type ? this.props.type : "text"}`
                } 
                placeholder={
                    `${this.props.placeholder ? this.props.placeholder : "Enter your text"}`
                }
                value={this.props.value}
                onChange={(e) => {
                    this.updateValue(e.target.value, this.props.valueType)
                }}
            />
        )
    }
}

export default Index;