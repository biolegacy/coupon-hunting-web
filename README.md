Project structure:

All of the code resides in `/src` fodler.

# Routes

If you go the `App.js` in `/src` folder, you can find all of the routes the app has currently.

## Components

Independent component that's being used in a multiple pages placed in here.

## Pages

Main pages of the web application. There are subfolder in it that's categorizing it by the `purpose/use`.

> For example:
> There is a subfolder named `Company` in Pages. 
> Usually, the `index.js` files in the subfolder is for list view.
> For indivudual object preview, there is a file called `view.js`.
> Use case: `index.js` is our entry for companies which shows us the list of companies
> When we want to view a specific company, then the app navigates us to another page `view.js`

## Style

All of the (CSS) styles resides in there. There are 2 subfolders `css`, `scss`.
This project is using compass to compile the `scss` to `css`. 

> You can check the configuration in the `config.rb` file in `/src/style` directory.

## Service

All the necessary part for the Redux integration resides here. There are 2 sub folders named `Reducers` and `Store`.

Pretty self explanatory, All of the `Reducers` resides in the `/src/service/reducer`.
Store that handles the reducers resides in the `/src/service/store`.

> If you have no understanding about Redux. It's use on this application.
> React has independent state management system which means every single Page has their own page and they don't share 
> state `Horizontally`. However, there is a way to share state `Vertically` via `Props`
> So, the main use for the `Redux` in our case is to have unified state management control that wraps the whole
> application and share the necessary information across the application
> ## Example
> Let's say user is at the `Login` page and he enters his credentials to login. When the credentials are valid the
> server returns a token for the `User`. When it comes to token, while the session is ongoing, when the user tries to do
> something (`buy`,`update information`, etc...) whatever the page the user in we need the user's `token` for the
> API calls. If we don't have a state management controller like `Redux`, we have to pass the token to page to page to
> keep it consistent, however if we are using `redux`, we can just get the `token` from any page when it's necessary.

> Pay attention: All of the components has their own state, redux state doesn't replace the components state,
> It works more like an extension.

## Utility

Functions those are being used in a multiple pages, are implemented here.

